import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { useEffect, useState } from "react";

export const useQuery = <T, M = any>({
  fetcher,
  enabled = true,
  initialData = null,
  ...rest
}: {
  fetcher: <T>(params: any) => Promise<T>;
  enabled?: boolean;
  initialData?: T | null;
} & Partial<FetcherBuilderParams<T, M>>) => {
  const [data, setData] = useState<T | null>(initialData);
  const [error, setError] = useState<Error | null>(null);
  const [status, setStatus] = useState<'idle' | 'loading' | 'error'>('idle');

  async function fetchData(){
    if (!enabled) return;

    setStatus('loading');
    
    try {
      const responseData = await fetcher<T>({ ...rest });
      setStatus('idle');
      setData(responseData);
    } catch (error: unknown) {
      setStatus('error');
      setError(error as Error);
    }
  }

  useEffect(() => {
    fetchData();
  }, [])

  return {
    data,
    refetch: fetchData,
    status,
    error,
    isLoading: status === 'loading',
    isError: status === 'error',
    isSuccess: status === 'idle',
  }
}
