import { useState } from "react";

import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";

export const useMutations = <T, M = any>({
  fetcher,
  enabled = true,
  initialData = null,
  ...rest
}: {
  fetcher: <T>(params: any) => Promise<T>;
  enabled?: boolean;
  initialData?: T | null;
} & Partial<FetcherBuilderParams<T, M>>) => {
  const [data, setData] = useState<T | null>(null);
  const [error, setError] = useState<Error | null>(null);
  const [status, setStatus] = useState<'idle' | 'loading' | 'error'>('idle');

  async function fetchMutation(body: M){
    if (!enabled) return;

    setStatus('loading');

    try {
      const responseData = await fetcher<T>({ ...rest, body });

      setStatus('idle');
      setData(responseData);
    } catch (error: unknown) {
      setStatus('error');
      setError(error as Error);
    }
  }

  return {
    data,
    mutate: fetchMutation,
    status,
    error,
    isLoading: status === 'loading',
    isError: status === 'error',
    isSuccess: status === 'idle',
  }
}
