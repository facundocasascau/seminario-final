export const PRIMARY_BRAND = '#800140';
export const HOVER_PRIMARY_BRAND = '#267D82';
export const SECONDARY_BRAND = '#800140';
// export const HOVER_SECONDARY_BRAND = '#3CB574';
export const WHITE = '#FFFFFF';

export const PRIMARY_TITLE_TEXT = '#045B95';
export const SECONDARY_TITLE_TEXT = '#85B0CC';
export const BLACK_TEXT = '#393939';
export const LIGHT_GRAY_TEXT = '#979797';
export const DARK_GRAY_TEXT = '#5E5E5E';
export const GRAY_DIVIDER = '#ECECEC';
export const GRAY_BACKGROUND_HOVER = '#EAEAEA';

export const INPUT_BACKGROUND = '#F1FCFF';
export const INPUT_BACKGROUND_ERROR = '#FFEEEE';
export const INPUT_COLOR_ERROR = '#BB1A1A';
export const INPUT_PLACEHOLDER_ERROR = '#E7B7B7';
export const MAIN_BACKGROUND = '#FAFAFA';
export const TAB_SELECTED = '#BCEBFF';

export const PAPER_WARNING = '#FFEFD7';
export const PAPER_WARNING_TEXT = '#BA812B';
export const PAPER_INFO = '#F0F0F0';

export const CRITICAL_TEXT = '#BB1A1A';
export const CRITICAL_BACKGROUND = '#FFEEEE';

export const SIDEMENU_SCROLLBAR = '#0f2d4033';
export const SIDEMENU_SCROLLBAR_HOVER = '#0f2d4055';

export const TRANSPARENT_BLACK_10 = 'rgba(0, 0, 0, 0.1)';
export const TRANSPARENT_BLACK_20 = 'rgba(0, 0, 0, 0.2)';
export const TRANSPARENT_BLACK_60 = 'rgba(0, 0, 0, 0.6)';
export const TRANSPARENT = 'transparent';