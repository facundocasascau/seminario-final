// import parseToCamelCase from 'camelcase-keys';
// import parseToSnakeCase from 'snakecase-keys';

export type StatusMessagesType = {
  [key: string]: string;
};

// TODO: remove eslint disable rule
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export type GetDataType<T> =
  | {
      raw: boolean;
      formatter: <T>(data: any) => T;
    }
  | boolean;

export const DefaultDataFormatter = <T>(data: any) =>
  /* parseToCamelCase(data, { deep: true }) */ data as T;

export type ParseDataType =
  | {
      raw: boolean;
      parser: (data: any) => any;
    }
  | boolean;

export const DefaultBodyParser = (data: any) => /* parseToSnakeCase(data, { deep: true }) */ data;

const DefaultStatusMessages: StatusMessagesType = {
  200: 'Petición realizada correctamente',
  201: 'Creado Correctamente',
  202: 'Aceptado',
  204: 'No se ha encontrado ningún elemento',
  400: 'Petición inválida',
  401: 'No autorizado',
  403: 'Acceso prohibido',
  404: 'No encontrado',
  405: 'Método no permitido',
  406: 'No aceptado',
  409: 'Ya existe un elemento con ese nombre',
  500: 'Error interno del servidor',
  501: 'No implementado',
  502: 'Error de red',
  503: 'Servicio no disponible',
  504: 'Tiempo de espera excedido'
};

function handleMessageErrors(response: Response, statusMessages?: StatusMessagesType) {
  const finalStatusMessages = { ...DefaultStatusMessages, ...statusMessages };
  const statusMessage = finalStatusMessages[response.status];

  if (!response.ok) throw new Error(statusMessage) || 'Error desconocido';
  return response;
}

export async function handleApiResponse<T>(
  response: Response,
  getData: GetDataType<T> = { raw: false, formatter: DefaultDataFormatter },
  statusMessages?: StatusMessagesType
) {
  handleMessageErrors(response, statusMessages);
  if (typeof getData === 'boolean') {
    if (getData) return await response.json();
    else return;
  } else {
    if (getData.raw) return await response.json();
    else if (getData.formatter) return await response.json().then(getData.formatter);
  }
  throw new Error('getData is not valid');
}

export const handleError = (err: any) => {
  console.error('ERROR: ', err);
  return Promise.reject(err);
};

export async function sendRequest(
  url: string,
  method: string,
  config: RequestInit,
  body?: any,
  bodyParser: ParseDataType = { raw: false, parser: DefaultBodyParser }
) {
  let bodyParsed = undefined;
  if (body) {
    if (typeof bodyParser === 'boolean') {
      if (bodyParser) {
        bodyParsed = body;
      }
    } else {
      if (bodyParser.raw) {
        bodyParsed = body;
      } else if (bodyParser.parser) {
        bodyParsed = JSON.stringify(bodyParser.parser(body));
      } else {
        bodyParsed = JSON.stringify(body);
      }
    }
  }
  return fetch(url, {
    ...config,
    method,
    body: bodyParsed
  }).catch(handleError);
}
