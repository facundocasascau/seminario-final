import { API_URLS } from 'constants/urls';
import {
  DefaultBodyParser,
  DefaultDataFormatter,
  GetDataType,
  handleApiResponse,
  ParseDataType,
  sendRequest as handleRequest,
  StatusMessagesType
} from './handlers';

const baseUrl = API_URLS.BASE;

// fetch configuration object
const config: RequestInit = {
  mode: 'cors',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};

export type ApiBaseType<T> = {
  url: string;
  getData?: GetDataType<T>;
  statusMessages?: StatusMessagesType;
  bodyParser?: ParseDataType;
};

type ApiType = {
  get: <T>(options: object & ApiBaseType<T>) => Promise<T>;
  post: <T>(options: { body?: any } & ApiBaseType<T>) => Promise<T>;
  put: <T>(options: { body: any } & ApiBaseType<T>) => Promise<T>;
  patch: <T>(options: { body: any } & ApiBaseType<T>) => Promise<T>;
  delete: <T>(options: { body?: any } & ApiBaseType<T>) => Promise<T>;
};

const Api = (baseUrl: string, config: RequestInit) =>
  ({
    get: async <T>({
      url,
      getData = { raw: false, formatter: DefaultDataFormatter },
      statusMessages
    }: object & ApiBaseType<T>) => {
      const parsedUrl = url.length > 0 && url[0] === '/' ? url.slice(1) : url;
      const response = await handleRequest(`${baseUrl}/${parsedUrl}`, 'GET', config);
      return handleApiResponse<T>(response, getData, statusMessages);
    },
    post: async <T>({
      url,
      body,
      getData = false,
      statusMessages,
      bodyParser = { raw: false, parser: DefaultBodyParser }
    }: { body?: any } & ApiBaseType<T>) => {
      const parsedUrl = url.length > 0 && url[0] === '/' ? url.slice(1) : url;
      const response = await handleRequest(
        `${baseUrl}/${parsedUrl}`,
        'POST',
        config,
        body,
        bodyParser
      );
      return handleApiResponse<T>(response, getData, statusMessages);
    },
    put: async <T>({
      url,
      body,
      getData = false,
      statusMessages,
      bodyParser = { raw: false, parser: DefaultBodyParser }
    }: { body: any } & ApiBaseType<T>) => {
      const parsedUrl = url.length > 0 && url[0] === '/' ? url.slice(1) : url;
      const response = await handleRequest(
        `${baseUrl}/${parsedUrl}`,
        'PUT',
        config,
        body,
        bodyParser
      );
      return handleApiResponse<T>(response, getData, statusMessages);
    },
    patch: async <T>({
      url,
      body,
      getData = false,
      statusMessages,
      bodyParser = { raw: false, parser: DefaultBodyParser }
    }: { body: any } & ApiBaseType<T>) => {
      const parsedUrl = url.length > 0 && url[0] === '/' ? url.slice(1) : url;
      const response = await handleRequest(
        `${baseUrl}/${parsedUrl}`,
        'PATCH',
        config,
        body,
        bodyParser
      );
      return handleApiResponse<T>(response, getData, statusMessages);
    },
    delete: async <T>({
      url,
      body,
      getData = false,
      statusMessages
    }: { body: any } & ApiBaseType<T>) => {
      const parsedUrl = url.length > 0 && url[0] === '/' ? url.slice(1) : url;
      const response = await handleRequest(`${baseUrl}/${parsedUrl}`, 'DELETE', config, body);
      return handleApiResponse<T>(response, getData, statusMessages);
    }
  } as ApiType);

const api = Api(baseUrl, config);

export { api as default, api };
