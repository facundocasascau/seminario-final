/**
 * @param {Record<string, any>} params
 * @returns {string}
 * @description
 * This function takes a params object and returns a string of query params.
 *
 * Example:
 * ```
 *  const params = { username: 'octocat', page: 1 };
 *  const url = 'https://api.github.com/users';
 *  const queryParams = parseUrlQueryParams(params);
 *  const parsedUrl = `${url}?${queryParams}`;
 *  // parsedUrl = 'https://api.github.com/users?username=octocat&page=1'
 */
export const parseUrlQueryParams = (params: Record<string, any>) => {
  return Object.keys(params).reduce((acc, key) => {
    const value = params[key];
    if (value !== undefined && value !== null) {
      acc.push(`${key}=${value}`);
    }
    return acc;
  }, [] as string[]).join('&');
}
