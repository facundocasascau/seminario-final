/**
 * @param {string} url
 * @param {Record<string, any>} params
 * @returns {string}
 * @description
 * This function takes a url and a params object and replaces the url params with the values from the params object.
 *
 * Example:
 * ```
 *  const url = 'https://api.github.com/users/{username}';
 *  const params = { username: 'octocat' };
 *  const parsedUrl = parseUrlParams(url, params);
 *  // parsedUrl = 'https://api.github.com/users/octocat'
 *```
 */
export const parseUrlParams = (url: string, params: Record<string, any>) => {
  let parsedUrl = url;
  Object.keys(params).forEach((key) => {
    const value = params[key];
    if (value !== undefined && value !== null) {
      parsedUrl = parsedUrl.replace(`{${key}}`, value);
    }
  });

  return parsedUrl;
}
