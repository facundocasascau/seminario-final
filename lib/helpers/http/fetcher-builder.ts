import { ApiBaseType } from "lib/api/instances";
import { parseUrlParams, parseUrlQueryParams } from "../url";

export type FetcherBuilderParams<T, M = any> = {
  queryParams?: Record<string, any>;
  urlParams?: Record<string, any>;
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  body?: M,
} & ApiBaseType<T>;

export const fetcherBuilder = <T, M>({
  url,
  queryParams,
  urlParams,
  body,
  method = 'GET',
  ...rest
}: FetcherBuilderParams<T, M>) : {
  apiRequestOptions: { body?: M } & ApiBaseType<T>,
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
} => {
  const params = queryParams ? parseUrlQueryParams(queryParams) : '';
  const parsedUrl = urlParams ? parseUrlParams(url, urlParams) : url;
  const apiUrl = `${parsedUrl}?${params}`;
  const apiRequestOptions = {
    url: apiUrl,
    body: body || {} as M,
    ...rest,
  }
  return {
    apiRequestOptions,
    method,
  };
}
