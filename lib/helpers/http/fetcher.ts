import { api } from "lib/api/instances";
import { fetcherBuilder, FetcherBuilderParams } from "./fetcher-builder";

export type FetcherParams<T, M = any> = {
  initialValues?: T;
  enabled?: boolean;
} & FetcherBuilderParams<T, M>;

export const fetcher = async <T, M = any>({
  initialValues,
  enabled = true,
  ...fetcherBuilderParams
}: FetcherParams<T, M>): Promise<T> => {
  if (!enabled) return initialValues as T;

  const { apiRequestOptions: {
    body,
    ...options
  }, method } = fetcherBuilder<T, M>(fetcherBuilderParams);

  if(!options.url) return initialValues as T;
  
  switch (method) {
    case 'POST':
      return api.post<T>({ ...options, body});
    case 'PUT':
      return api.put<T>({ ...options, body});
    case 'PATCH':
      return api.patch<T>({ ...options, body});
    case 'DELETE':
      return api.delete<T>({ ...options, body});
    default:
      return api.get<T>(options);
  }
}
