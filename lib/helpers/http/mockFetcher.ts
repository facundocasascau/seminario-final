import { DEFAULT_MOCK_TIMEOUT } from 'constants/timeouts';
import { FetcherBuilderParams } from './fetcher-builder';

export const mockFetcher = <T>(mockData: T, mockTimeout?: number) => {
  const timeout = mockTimeout || DEFAULT_MOCK_TIMEOUT;

  // TODO: remove eslint disable rule
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return <T, M>(_params: FetcherBuilderParams<T, M>) => {
    return new Promise<T>(resolve => setTimeout(resolve, timeout, mockData));
  };
};
