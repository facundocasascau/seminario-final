import { useQuery } from "hooks/useQuery";
import { fetcher } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { dailyOperations } from "mocks";

const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/journals/daily-operations', ...params });

export const useGetDailyOperations = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof dailyOperations>({
    fetcher: usersFetcher,
    ...options,
  });
}
