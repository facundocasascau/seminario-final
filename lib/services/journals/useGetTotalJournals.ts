import { useQuery } from "hooks/useQuery";
import { fetcher } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { totalJournal } from "mocks";

const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/journals/summary', ...params });

export const useGetTotalJournals = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof totalJournal>({
    fetcher: usersFetcher,
    ...options,
  });
}
