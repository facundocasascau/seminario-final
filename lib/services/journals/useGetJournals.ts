import { useQuery } from "hooks/useQuery";
import { fetcher } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { journals } from "mocks";

const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/journals', ...params });

export const useGetJournals = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof journals>({
    fetcher: usersFetcher,
    ...options,
  });
}
