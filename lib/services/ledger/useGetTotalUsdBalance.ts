import { useQuery } from "hooks/useQuery";
import { fetcher } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { usdBalance } from "mocks";

const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/ledger/total-usd-balance', ...params });

export const useGetTotalUsdBalance = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof usdBalance>({
    fetcher: usersFetcher,
    ...options,
  });
}
