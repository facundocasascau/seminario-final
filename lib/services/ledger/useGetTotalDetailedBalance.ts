import { useQuery } from "hooks/useQuery";
import { fetcher } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { totalDetailedBalance } from "mocks";

const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/ledger/total-detailed-balance', ...params });

export const useGetTotalDetailedBalance = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof totalDetailedBalance>({
    fetcher: usersFetcher,
    ...options,
  });
}
