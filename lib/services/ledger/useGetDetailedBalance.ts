import { useQuery } from "hooks/useQuery";
import { fetcher } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { detailedBalance } from "mocks";

const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/ledger/detailed-balance', ...params });

export const useGetDetailedBalance = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof detailedBalance>({
    fetcher: usersFetcher,
    ...options,
  });
}
