import { useQuery } from "hooks/useQuery";
import { fetcher/* , mockFetcher */ } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { users } from "mocks/users";

// const usersFetcher = mockFetcher(users);
const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/users', ...params });

export const useGetUsers = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof users>({
    fetcher: usersFetcher,
    ...options,
  });
}
