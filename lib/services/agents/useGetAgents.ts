import { useQuery } from "hooks/useQuery";
import { fetcher/* , mockFetcher */ } from "lib/helpers/http";
import { FetcherBuilderParams } from "lib/helpers/http/fetcher-builder";
import { agents } from "mocks/agents";

const usersFetcher = <T>(params: any) => fetcher<T>({ url: '/agents', ...params });

export const useGetAgents = <T, M>(options?: {
  enabled?: boolean,
} & Partial<FetcherBuilderParams<T, M>>) => {
  return useQuery<typeof agents>({
    fetcher: usersFetcher,
    ...options,
  });
}
