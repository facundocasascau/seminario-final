export interface Cheque {
  id: number;
  serialNumber: string;
  issueDate: string;
  expirationDate: string;
  termDate: string | undefined;
  nominalValue: string;
  bankingAccountNumber: string;
  bank: string;
  branch: string;
  status: string;
  operations: {
    id: number;
    operationType: number;
    operationDate: string;
    amount: string;
    comments: string;
  }[];
}
