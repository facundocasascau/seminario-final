export type Person = {
    id: number;
    companyId: number;
    firstName: string;
    lastName: string;
    documentNumber?: string;
    sex?: string;
    email?: string;
    phone?: string;
    address?: string;
  };
  