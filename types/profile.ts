import { Person } from './person';

export type Profile = {
  id: number;
  companyId: number;
  person: Person;
  profileType: ProfileType;
};

export type ProfileType = {
  id: number;
  description: string;
};
