# Lang translation

## schemas

 - tenancy
 - auth
 - audit
 - finance
 - trading
 - accounting

## entities & fields

### company: empresa
 - name: nombre
 - code: código
 - created_on_utc: fecha de creación
 - modified_on_utc: fecha de modificación
 - deleted_on_utc: fecha de desactivación
 - deleted: eliminado

### user: usuario
 - username: nombre de usuario / usuario
 - email: correo electrónico / email
 - password: contraseña
 - reset_password: restaurar contraseña
 - retry_pasword_count: conteo de reintento de contraseña
 - enabled: habilitado
 - created_on_utc: fecha de creación
 - modified_on_utc: fecha de modificación
 - deleted_on_utc: fecha de desactivación
 - deleted: eliminado

### person: persona
 - first_name: nombre / nombres
 - last_name: apellido / apellidos
 - document_number: número de documento
 - sex: sexo
 - email: correo electrónico / email
 - phone: teléfono
 - address: dirección
 - created_on_utc: fecha de creación
 - modified_on_utc: fecha de modificación

### profile: perfil
 - created_on_utc: fecha de creación
 - modified_on_utc: fecha de modificación
 - deleted_on_utc: fecha de desactivación
 - deleted: eliminado

### profile_type: tipo de perfil
 - description: descripción

### box: caja
 - description: descripción
 - created_on_utc: fecha de creación
 - modified_on_utc: fecha de modificación
 - deleted_on_utc: fecha de desactivación
 - deleted: eliminado

### role: rol
 - description: descripción

### permission: permiso
 - description: descripción

### action: acción
 - description: descripción

### resource: recurso
 - description: descripción

### entity: entidad
 - description: descripción

### entity_audit_log: registro de auditoría
 - date_time_on_utc: fecha y hora
 - old_values: viejos valores
 - new_values: nuevos valores

### currency_type: tipo de divisa
 - description: descripción

### currency: divisa
 - name: nombre
 - code: código

### account_type: tipo de cuenta
 - description: descripción

### account: cuenta
 - created_on_utc: fecha de creación
 - modified_on_utc: fecha de modificación
 - deleted_on_utc: fecha de desactivación
 - deleted: eliminado

### investment_account: cuenta de plazo fijo
 - investment_account_number: número de cuenta de plazo fijo

### investment_configuration: configuración de plazo fijo
 - interest_rate: tasa de interés
 - start_date: fecha de inicio
 - end_date: fecha de finalización
 - enabled: habilitado

### investment_ledger: libro de plazos fijos
 - balance: balance
 - interest_rate: tasa de interés
 - date_time_on_utc: fecha y hora
 - daily_interest_rate: tasa de interés diaria

### current_account: cuenta corriente / CTA CTE
 - current_account_number: número de cuenta corriente / n CTA CTE

### current_account_balance: balance de cuenta corriente
 - balance: balance

### external_account: cuenta externa
 - banking_account_number: número de cuenta
 - single_banking_code: CBU
 - single_virtual_code: CVU
 - external_account_alias: alias
 - crypto_address: dirección crypto
 - crypto_network: red crypto

### financial_entity: entidad financiera
 - description: descripción
 - available_currency_types: tipos de divisa disponibles (FIAT/Crypto)

### financial_type: tipo de entidad financiera
 - description: descripción

### cheque
 - serial_number: número de serie
 - financial_entity_branch: sucursal
 - issuer: emisor
 - nominal_value: valor nominal
 - issue_date: fecha de emisión
 - term_date: fecha de pago diferido (a revisar traducción)
 - expiration_date: fecha de expiración
 - is_post_dated: es de pago diferido

### operation_type: tipo de operación
 - description: descripción

### operation_subtype: subtipo de operación
 - description: descripción


### operation: operación
 - customer_id: cliente
 - broker_id: comisionista
 - created_on_utc: fecha de creación
 - operation_number: número de operación
 - comment: comentarios

### financial_transaction: transacción
 - currency_id_source: divisa de origen
 - currency_id_destination: divisa de destino
 - amount_source: monto de origen
 - amount_destination: monto de destino
 - exchange_rate: tipo de cambio / TC
 - is_exchange_rate_percent: es tipo de cambio porcentual
 - account_type_id_source: tipo de cuenta de origen
 - account_type_id_destination: tipo de cuenta de destino
 - box_id_source: caja de origen
 - box_id_destination: caja de destino
 - account_id_source: cuenta de origen
 - account_id_destination: cuenta de destino
 - third_party_account_source: cuenta de terceros de origen
 - third_party_account_destination: cuenta de terceros de destino
 - created_on_utc: fecha de creación

### ledger: libro diario
 - currency_type_id: tipo de divisa
 - exchanged_currency_type_id: tipo de divisa intercambiada
 - exchange_rate: tipo de camio
 - amount: monto
 - exchanged_amount: monto intercambiado
 - date: fecha

### subledger: libro auxiliar
 - box_id: caja
 - ledger_id: libro diario
