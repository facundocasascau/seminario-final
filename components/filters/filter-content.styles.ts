import { styled } from '@mui/material';
import { GridV2 } from 'components/container';

export const FilterContainer = styled(GridV2)(({ theme }) => ({
  [theme.breakpoints.down('md')]: {
    flexDirection: 'column'
  },
  [theme.breakpoints.up('md')]: {
    flexDirection: 'row'
  }
}));

export const Filters = styled(GridV2)(() => ({
  border: '1px solid lightgray',
  borderRadius: '15px'
}));
