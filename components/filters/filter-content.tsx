import { ReactNode } from 'react';
import { FilterContainer, Filters } from './filter-content.styles';
import { GridV2 } from 'components/container';

export interface FilterContentProps {
  filters: ReactNode;
  children: ReactNode;
}

export const Index = ({ filters, children }: FilterContentProps) => {
  return (
    <FilterContainer
      container
      spacing={2}
    >
      <Filters
        xs={12}
        sm={12}
        md={2}
      >
        {filters}
      </Filters>
      <GridV2
        xs={12}
        sm={12}
        md={8}
      >
        {children}
      </GridV2>
    </FilterContainer>
  );
};
