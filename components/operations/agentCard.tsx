import { DataList } from 'components/list';
import React from 'react';
import { Profile } from 'types';

export const Index = ({
  agent,
  type
}: {
  agent: Profile | undefined;
  type: 'customer' | 'broker';
}) => {
  const title = type === 'customer' ? 'Cliente:' : 'Comisionista:';
  const info = buildCardInfo(agent, title);
  return <DataList items={info} />;
};

const buildCardInfo = (agent: Profile | undefined, title: string) => {
  if (agent) {
    return [
      {
        primary: (
          <>
            {title} <b>{`${agent.person.firstName} ${agent.person.lastName}`}</b>
          </>
        ),
        secondary: <>{buildAgentInfo(agent)}</>
      }
    ];
  }
  return [
    {
      primary: (
        <>
          {title} <b>N/A</b>
        </>
      )
    }
  ];
};

const buildAgentInfo = (agent: Profile) => {
  const { person } = agent;
  const data: { label: string; value: any }[] = [];

  if (person.documentNumber) {
    data.push({ label: 'DNI', value: person.documentNumber });
  }
  if (person.phone) {
    data.push({ label: 'Teléfono', value: person.phone });
  }
  if (person.email) {
    data.push({ label: 'Email', value: person.email });
  }

  if (data.length === 0) {
    return undefined;
  }

  return (
    <>
      {data.map((item, index) => {
        if (index > 0) {
          return (
            <React.Fragment key={index}>
              <br />
              {`${item.label}: ${item.value}`}
            </React.Fragment>
          );
        }
        return <React.Fragment key={index}>{`${item.label}: ${item.value}`}</React.Fragment>;
      })}
    </>
  );
};
