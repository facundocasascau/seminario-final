import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { Profile } from 'types';
import MenuItem from '@mui/material/MenuItem';
import { Modal } from 'components/modal';
import { Subtitle, Text } from 'components/typography';
import { ContainerControl } from 'components/formControl/formControl.styles';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { Button } from 'components/button';

const agents: Profile[] = [
  {
    id: 1,
    companyId: 1,
    person: {
      id: 1,
      companyId: 1,
      firstName: 'Juan Octavio',
      lastName: 'Sanchez del Corazón de la Rivera',
      documentNumber: '99099123',
      sex: 'Masculino',
      email: 'juano@gmail.com',
      phone: '3813333023',
      address: 'Fray Mamerto Esquiú 230'
    },
    profileType: {
      id: 1,
      description: 'Agente'
    }
  },
  {
    id: 2,
    companyId: 1,
    person: {
      id: 2,
      companyId: 1,
      firstName: 'Ana',
      lastName: 'Terán',
      documentNumber: '3099123',
      sex: 'Femenino',
      email: 'anita@gmail.com',
      phone: '3814378802',
      address: 'Av. Belgrano 2000'
    },
    profileType: {
      id: 1,
      description: 'Agente'
    }
  }
];

export const Index = ({
  open,
  setOpen,
  setAgent,
  type
}: {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  setAgent: Dispatch<SetStateAction<Profile | undefined>>;
  type: 'customer' | 'broker';
}) => {
  const componentInfo = agentType[type];
  const [agentModal, setAgentModal] = useState<Profile>();
  const agentOptions = agents.map((ag, index) => {
    return (
      <MenuItem
        key={index}
        value={ag.id}
      >
        {`${ag.person.firstName} ${ag.person.lastName}`}
      </MenuItem>
    );
  });

  const handleSelect = (ev: SelectChangeEvent) => {
    const id: number = +ev.target.value;
    const agent = agents.find((agent: Profile) => agent.id === id);
    setAgentModal(agent);
  };

  const handleAdd = () => {
    setAgent(agentModal);
    setOpen(false);
    setAgentModal(undefined);
  };

  useEffect(() => {
    if (open === false) {
      setAgentModal(undefined);
    }
  }, [open]);

  return (
    <Modal
      open={open}
      setOpen={setOpen}
      title={componentInfo.modalTitle}
    >
      <ContainerControl>
        <FormControl fullWidth>
          <InputLabel>{componentInfo.inputLabel}</InputLabel>
          <Select
            name={componentInfo.inputName}
            label={componentInfo.inputLabel}
            onChange={handleSelect}
          >
            {agentOptions}
          </Select>
        </FormControl>
      </ContainerControl>
      <Text style={{ padding: '20px 0' }}>
        {agentModal && (
          <>
            <Subtitle>{`${agentModal.person.firstName} ${agentModal.person.lastName}`}</Subtitle>
            <Text>{`DNI: ${agentModal.person.documentNumber}`}</Text>
            <Text>{`Teléfono: ${agentModal.person.phone}`}</Text>
            <Text>{`Email: ${agentModal.person.email}`}</Text>
            <div
              style={{
                display: 'inline-flex',
                justifyContent: 'flex-end',
                flexDirection: 'row',
                width: '100%',
                gap: '1rem',
                paddingTop: '20px'
              }}
            >
              <Button
                sx={{ width: '120px' }}
                size="medium"
                variant="contained"
                color="primary"
                onClick={handleAdd}
              >
                Agregar
              </Button>
            </div>
          </>
        )}
      </Text>
    </Modal>
  );
};

const agentType = {
  customer: {
    modalTitle: 'Agregar cliente',
    inputLabel: 'Buscar cliente',
    inputName: 'customer'
  },
  broker: {
    modalTitle: 'Agregar comisionista',
    inputLabel: 'Buscar comisionista',
    inputName: 'broker'
  }
};
