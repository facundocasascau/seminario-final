import { GridContainer, GridV2, Paper } from 'components/container';
import { InputSelect } from 'components/formControl';
import { FormControlProps } from './interfaces';
import { flexGrid } from './styles';
import React from 'react';
import { AgentSection } from '.';
import { configurationTypes, operationSubTypes, operationTypes } from 'mocks';
import { UseFormWatch } from 'react-hook-form';

export const Index = ({ control, watch }: FormControlProps & { watch: UseFormWatch<any> }) => {
  const opType = watch('opType');
  const configuration = !!opType ?
    configurationTypes.filter(op => op.type_id === opType)?.map(op => op.subtype_id) || [] : [];
  const opSubtypes = operationSubTypes.filter(osp => configuration.includes(osp.id)) || [];
  return (
    <Paper>
      <GridContainer>
        <GridV2 xs={3}>
          <InputSelect
            name="opType"
            control={control}
            label="Tipo"
            options={operationTypes
              .filter(op => op.id > 1)
              .map(op => ({ value: op.id, label: op.description }))}
          />
        </GridV2>
        <GridV2 xs={3}>
          <InputSelect
            name="opSubtype"
            control={control}
            label="Subtipo"
            options={!!opType ? opSubtypes.map(op => ({ value: op.id, label: op.description })) : []}
          />
        </GridV2>
        <GridV2
          xs={3}
          sx={flexGrid}
        >
          <AgentSection type="customer" />
        </GridV2>
        <GridV2
          xs={3}
          sx={flexGrid}
        >
          <AgentSection type="broker" />
        </GridV2>
      </GridContainer>
    </Paper>
  );
};
