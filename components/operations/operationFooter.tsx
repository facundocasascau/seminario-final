import { GridContainer, GridV2, Paper } from 'components/container';
import { FormControlProps } from './interfaces';
import { DataGrid } from 'components/dataGrid';
import { GridColDef } from '@mui/x-data-grid';
import { InputText } from 'components/formControl';

export const Index = ({ control }: FormControlProps) => {
  const balanceColumns: GridColDef[] = [
    {
      field: 'debit',
      headerName: 'Debe',
      flex: 1
    },
    {
      field: 'credit',
      headerName: 'Haber',
      flex: 1
    }
  ];
  return (
    <Paper style={{ padding: 0 }}>
      <GridContainer>
        <GridV2 xs={3}>
          <DataGrid
            columns={balanceColumns}
            rows={[{ id: 1, debit: 'ARS $500.000', credit: 'USD $1.000' }]}
            sortable={false}
            hideFooter
            style={{ border: 0 }}
          />
        </GridV2>
        <GridV2
          xs={9}
          padding={'25px'}
        >
          <InputText
            name={'comments'}
            control={control}
            label={'Comentarios'}
            multiline
            rows={4}
          />
        </GridV2>
      </GridContainer>
    </Paper>
  );
};
