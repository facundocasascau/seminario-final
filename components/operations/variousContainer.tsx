import { Various } from '.';
import { FormControlProps } from './interfaces';
import { Paper } from 'components/container';

export const Index = ({ control }: FormControlProps) => {
  return (
    <Paper>
      <Various control={control} />
    </Paper>
  );
};
