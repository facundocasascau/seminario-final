import { GridContainer, GridV2, Paper } from 'components/container';
import { InputSelect } from 'components/formControl';
import { FormControlProps } from './interfaces';
import { flexGrid } from './styles';
import React from 'react';
import { AgentSection } from '.';
import { configurationMaterias, turno, materias, anio } from 'mocks';
import { UseFormWatch } from 'react-hook-form';

export const Index = ({ control, watch }: FormControlProps & { watch: UseFormWatch<any> }) => {
  const turnosTypes = watch('turno');
  const configuration = !!turnosTypes ?
  configurationMaterias.filter(t => t.turno_id === turnosTypes)?.map(t => t.materia_id) || [] : [];
  const materiasTypes = materias.filter(osp => configuration.includes(osp.id)) || [];
  return (
    <Paper>
      <GridContainer>
        <GridV2 xs={3}>
          <InputSelect
            name="turno"
            control={control}
            label="Turno"
            options={turno
              .map(t => ({ value: t.id, label: t.description }))}
          />
        </GridV2>
        <GridV2 xs={3}>
          <InputSelect
            name="materias"
            control={control}
            label="Materias"
            options={!!turno ? materiasTypes.map(op => ({ value: op.id, label: op.description })) : []}
          />
        </GridV2>
        <GridV2 xs={3}>
          <InputSelect
            name="anio"
            control={control}
            label="Año Lectivo"
            options={!!turno ? anio.map(a => ({ value: a.id, label: a.description })).sort((a, b) => parseInt(b.label) - parseInt(a.label)) : []}
          />
        </GridV2>
      </GridContainer>
    </Paper>
  );
};
