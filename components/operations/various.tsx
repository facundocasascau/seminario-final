import { FormControlProps } from './interfaces';
import { GridContainer, GridV2 } from 'components/container';
import { InputSelect, InputText } from 'components/formControl';

import { currencies } from 'mocks';

export const Index = ({ control }: FormControlProps) => {
  const currencyOptions = currencies.map(x => {
    return { value: x.id, label: x.code };
  });
  return (
    <GridContainer>
      <GridV2 xs={1.5}>
        <InputSelect
          name="currencyOrigin"
          control={control}
          label="Moneda"
          options={currencyOptions}
        />
      </GridV2>
      <GridV2 xs={2}>
        <InputText
          name="amount"
          control={control}
          type="number"
          label="Monto"
        />
      </GridV2>
    </GridContainer>
  );
};
