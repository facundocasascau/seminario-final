import { Control } from 'react-hook-form';

export interface FormControlProps {
  control: Control<any, any>;
  opType?: string;
  opSubtype?: string;
}
