import IconButton from '@mui/material/IconButton';
import { Icon } from 'components/icons/icons';
import { useState } from 'react';
import { AgentCard, AgentModal } from '.';
import { Profile } from 'types';

export const Index = ({ type }: { type: 'customer' | 'broker' }) => {
  const [agent, setAgent] = useState<Profile>();

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleDelete = () => setAgent(undefined);

  return (
    <>
      <AgentModal
        open={open}
        setOpen={setOpen}
        type={type}
        setAgent={setAgent}
      />
      <AgentCard
        agent={agent}
        type={type}
      />
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {!agent ? (
          <IconButton onClick={handleOpen}>
            <Icon name="AddCircle" />
          </IconButton>
        ) : (
          <>
            <IconButton onClick={handleOpen}>
              <Icon name="Edit" />
            </IconButton>
            <IconButton onClick={handleDelete}>
              <Icon name="Cancel" />
            </IconButton>
          </>
        )}
      </div>
    </>
  );
};
