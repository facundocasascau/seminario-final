import styled from '@emotion/styled';
import Link from 'next/link';
import { SECONDARY_BRAND } from 'constants/mainTheme';

export const Span = styled.span`
  color: ${SECONDARY_BRAND};
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 14px;
  display: inline;
`;

export const LinkContainer = styled(Link)`
  text-decoration: none;

  :hover {
    text-decoration: underline;
  }
`;

export const Bold = styled.b`
  font-weight: 700;
`;
