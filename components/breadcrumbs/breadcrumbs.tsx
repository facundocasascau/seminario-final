import React from 'react';
import { Bold, LinkContainer, Span } from './breadcrumbs.styles';

export interface BreadcrumbsProps {
  links?: {
    name: string;
    href?: string;
  }[];
}

export function Index({ links }: BreadcrumbsProps) {
  return (
    <Span>
      {links?.map((value, index) => {
        if (index === links?.length - 1) {
          return <Bold key={index}>{value.name}</Bold>;
        }
        return (
          <React.Fragment key={index}>
            <LinkContainer
              href={value.href as string}
              passHref
            >
              {value.name}
            </LinkContainer>{' '}
            /{' '}
          </React.Fragment>
        );
      })}
    </Span>
  );
}
