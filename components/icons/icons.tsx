import MuiIcons from '@mui/icons-material';
import Warning from '@mui/icons-material/Warning';
import Close from '@mui/icons-material/Close';
import Add from '@mui/icons-material/Add';
import AddCircle from '@mui/icons-material/AddCircle';
import Edit from '@mui/icons-material/Edit';
import Cancel from '@mui/icons-material/Cancel';
import Delete from '@mui/icons-material/Delete';
import ReadMore from '@mui/icons-material/ReadMore';
import DoDisturb from '@mui/icons-material/DoDisturb';

type IconName = keyof typeof MuiIcons;

export const Icon = ({ name, ...rest }: { name: IconName }) => {
  const icons = {
    Warning,
    Close,
    Add,
    AddCircle,
    Edit,
    Cancel,
    Delete,
    ReadMore,
    DoDisturb
  };
  const Icon = icons[name as keyof typeof icons];
  return <Icon {...rest} />;
};
