import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import IconButton from '@mui/material/IconButton';
import { Icon } from 'components/icons/icons';
import { TitleContainer } from 'components/container';

const style = {
  padding: '0 15px 15px 15px',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  borderRadius: '5px',
  boxShadow: 24
};

interface ModalProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  children: React.ReactNode;
  title?: string;
  closable?: boolean;
  width?: number | string;
  minHeight?: number | string;
}

export const Index = ({
  open,
  setOpen,
  children,
  title,
  width,
  minHeight,
  closable = true
}: ModalProps) => {
  const handleClose = () => setOpen(false);

  return (
    <Modal
      open={open}
      closeAfterTransition
      slots={{ backdrop: Backdrop }}
      slotProps={{
        backdrop: {
          timeout: 500
        }
      }}
    >
      <Fade in={open}>
        <Box sx={{ ...style, width: width ?? '575px', minHeight: minHeight ?? 200 }}>
          <TitleContainer
            subtitle={title}
            action={
              closable && (
                <IconButton onClick={handleClose}>
                  <Icon name="Close" />
                </IconButton>
              )
            }
          />
          {children}
        </Box>
      </Fade>
    </Modal>
  );
};
