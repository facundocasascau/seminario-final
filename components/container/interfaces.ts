import { Grid2Props } from '@mui/material/Unstable_Grid2';
import { GridSpacing } from '@mui/system/Unstable_Grid';
import { ResponsiveStyleValue } from '@mui/system/styleFunctionSx';
import { ReactNode } from 'react';

export interface TitleContainerProps {
  title?: string | React.ReactNode;
  subtitle?: string | React.ReactNode;
  text?: string | React.ReactNode;
  action?: React.ReactNode;
}

export interface GridContainerProps {
  children: ReactNode;
  spacing?: ResponsiveStyleValue<GridSpacing>;
  columns?: ResponsiveStyleValue<number>;
}

export interface GridV2Props extends Grid2Props {
  children: ReactNode;
}
