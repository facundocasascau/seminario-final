import React from 'react';
import { TitleContainer, TitleContainerCol } from './titleContainer.styles';
import { TitleContainerProps } from './interfaces';
import { Subtitle, Title, Text } from 'components/typography';

export const Index = ({ title, subtitle, text, action }: TitleContainerProps) => {
  return (
    <TitleContainer>
      <TitleContainerCol>
        {title && <Title>{title}</Title>}
        {subtitle && <Subtitle>{subtitle}</Subtitle>}
        {text && <Text>{text}</Text>}
      </TitleContainerCol>
      {action && <TitleContainerCol>{action}</TitleContainerCol>}
    </TitleContainer>
  );
};
