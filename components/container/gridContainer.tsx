import Grid from '@mui/material/Unstable_Grid2';
import React from 'react';
import { GridContainerProps } from './interfaces';

export const Index = ({ children, spacing, columns }: GridContainerProps) => {
  return (
    <Grid
      container
      spacing={spacing ?? 2}
      columns={columns ?? { xs: 2, sm: 6, md: 12, lg: 12 }}
      width={'100%'}
    >
      {children}
    </Grid>
  );
};
