import Paper, { PaperProps } from '@mui/material/Paper';

export const Index = ({ children, ...props }: PaperProps) => {
  return (
    <Paper
      {...props}
      style={{
        ...props.style,
        display: 'flex',
        flexWrap: 'wrap',
        rowGap: '20px',
        padding: props.style?.padding ?? '20px',
        width: '100%'
      }}
      variant="outlined"
    >
      {children}
    </Paper>
  );
};
