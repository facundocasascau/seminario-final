import React from 'react';

export const Index = ({ children }: { children: React.ReactNode }) => {
  return <div style={{ display: 'flex', flexWrap: 'wrap', rowGap: '20px' }}>{children}</div>;
};
