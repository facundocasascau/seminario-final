import Grid, { Grid2Props } from '@mui/material/Unstable_Grid2';
import React from 'react';

export const Index = ({ children, ...props }: Grid2Props) => {
  return <Grid {...props}>{children}</Grid>;
};
