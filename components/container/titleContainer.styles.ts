import styled from '@emotion/styled';

export const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 15px 0 35px 0;

  @media (max-width: 1023px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

export const TitleContainerCol = styled.div`
  display: flex;
  flex-direction: column;
`;
