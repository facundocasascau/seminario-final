import React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

interface DataListProps {
  items: {
    primary?: React.ReactNode;
    secondary?: React.ReactNode;
  }[];
}

export const Index = ({ items }: DataListProps) => {
  if (!Array.isArray(items) || items.length === 0) {
    return <></>;
  }
  return (
    <List
      sx={{ width: '100%', maxWidth: 360, paddingTop: 0 }}
      dense
    >
      {items.map((item, index) => {
        return (
          <React.Fragment key={index}>
            <ListItem sx={{ paddingTop: 0, paddingBottom: 0 }}>
              <ListItemText
                primary={item.primary}
                secondary={item.secondary}
              />
            </ListItem>
          </React.Fragment>
        );
      })}
    </List>
  );
};
