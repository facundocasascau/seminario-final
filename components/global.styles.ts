import { css } from "@emotion/react";
import {
  MAIN_BACKGROUND,
  SECONDARY_BRAND,
  TRANSPARENT_BLACK_10,
  WHITE,
} from "constants/mainTheme";

export const GlobalStyles = css`
  main {
    background-color: ${WHITE};
  }
  a {
    color: inherit;
    text-decoration: none;
  }
  * {
    box-sizing: border-box;
  }
  html {
    padding: env(safe-area-inset);

    body {
      white-space: pre-line;
      overflow-x: hidden;
      overflow-y: overlay;
      padding: 0;
      margin: 0;
      background-color: ${MAIN_BACKGROUND};
      font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
        Monserrat, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans,
        Helvetica Neue, sans-serif;
      ::-webkit-scrollbar {
        width: 7px;
        height: 7px;
      }
      ::-webkit-scrollbar-thumb {
        border-radius: 50px;
        background: ${SECONDARY_BRAND};
      }
      ::-webkit-scrollbar-track {
        background: ${TRANSPARENT_BLACK_10};
      }

      /* Firefox */
      scrollbar-width: thin;
      scrollbar-color: ${SECONDARY_BRAND} ${TRANSPARENT_BLACK_10};
    }
  }
`;
