import Head from 'next/head';
import Image from 'next/image';
import { Roboto } from 'next/font/google';
import TimelineIcon from '@mui/icons-material/Timeline';
import SettingsIcon from '@mui/icons-material/Settings';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import MessageIcon from '@mui/icons-material/Message';
import SchoolIcon from '@mui/icons-material/School';
import {
  AppBar,
  Box,
  Divider,
  Drawer,
  Icon,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  SvgIcon,
  Toolbar
} from '@mui/material';
import Link from 'next/link';
import { PRIMARY_BRAND } from 'constants/mainTheme';
import { WHITE } from 'constants/mainTheme';
import logo from 'public/logo.png';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

const roboto = Roboto({
  weight: ['300', '400', '500', '700'],
  subsets: ['latin']
});

const LINKS = [

  { text: 'Materias', href: '/materias', icon: MenuBookIcon },
  { text: 'Calendario', href: '/operaciones', icon: CalendarMonthIcon },
  {
    text: 'Mensajes', href: '/registro-contable',icon: MessageIcon
  },
  { text: 'Boletín', href: '/cheques', icon: SchoolIcon },
  { text: 'Estadísticas', href: '/estadisticas', icon: TimelineIcon }
];

const PLACEHOLDER_LINKS = [
  { text: 'Salir', icon: LogoutIcon }
];

const DRAWER_WIDTH = 240;

export const Index = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <Head>
        <title>Plataforma EMDEI</title>
        <link
          rel="icon"
          href="/favicon.ico"
        />
      </Head>
      <div className={roboto.className}>
        <AppBar
          position="fixed"
          sx={{ zIndex: 2000 }}
        >
          <Toolbar sx={{ backgroundColor: PRIMARY_BRAND, display: 'flex', justifyContent: 'space-between' }}>
            <Link href="/">
              <Image
                alt="EMDEI Logo"
                placeholder="blur"
                blurDataURL="public/logo.png"
                height={60}
                src={logo}
                />
            </Link>
            <Link href="/" style={{ display: 'flex', alignItems: 'center' }}>
              <SvgIcon component={AccountCircleIcon} sx={{ fontSize: 40, color: 'white' }} />
            </Link>
          </Toolbar>
          
        </AppBar>
        <Drawer
          sx={{
            width: DRAWER_WIDTH,
            flexShrink: 0,
            '& .MuiDrawer-paper': {
              width: DRAWER_WIDTH,
              boxSizing: 'border-box',
              top: ['48px', '56px', '64px'],
              height: 'auto',
              bottom: 0,
              backgroundColor: PRIMARY_BRAND,
              color: WHITE
            }
          }}
          variant="permanent"
          anchor="left"
        >
          <Divider />
          <List>
            {LINKS.map(({ text, href, icon: Icon }) => (
              <ListItem
                key={href}
                disablePadding
              >
                <ListItemButton
                  component={Link}
                  href={href}
                >
                  <ListItemIcon>
                    <Icon style={{ color: 'white' }} />
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
          <Divider sx={{ mt: 'auto' }} />
          <List>
            {PLACEHOLDER_LINKS.map(({ text, icon: Icon }) => (
              <ListItem
                key={text}
                disablePadding
              >
                <ListItemButton>
                  <ListItemIcon>
                    <Icon style={{ color: 'white' }}/>
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Drawer>
        <Box
          sx={{
            flexGrow: 1,
            bgcolor: 'background.default',
            ml: `${DRAWER_WIDTH}px`,
            mt: '64px',
            p: 5,
            width: `calc(100vw - ${DRAWER_WIDTH}px)`,
            minHeight: 'calc(100vh - 64px)'
          }}
        >
          {children}
        </Box>
      </div>
    </>
  );
};
