import { DataGrid, DataGridProps } from '@mui/x-data-grid';
import React from 'react';
import { DataGridContainer } from './dataGrid.styles';
import LinearProgress from '@mui/material/LinearProgress';
import Stack from '@mui/material/Stack';

export interface CustomDataGridProps extends DataGridProps {
  sortable?: boolean;
}

export function Index({ columns, sortable, ...props }: CustomDataGridProps) {
  if (sortable === false) {
    columns = columns.map(x => {
      return { ...x, sortable };
    });
  }
  return (
    <DataGridContainer>
      <DataGrid
        {...props}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 10 }
          }
        }}
        autoHeight={true}
        slots={{
          loadingOverlay: LinearProgress,
          noRowsOverlay: () => (
            <Stack
              height={100}
              alignItems="center"
              justifyContent="center"
            >
              Sin datos
            </Stack>
          ),
          noResultsOverlay: () => (
            <Stack
              height={100}
              alignItems="center"
              justifyContent="center"
            >
              No hay resultados
            </Stack>
          ),
          ...props.slots
        }}
        pageSizeOptions={[5, 10, 20]}
        rowSelection={false}
        disableColumnMenu
        disableColumnFilter
        disableColumnSelector
        disableDensitySelector
      />
    </DataGridContainer>
  );
}
