import styled from '@emotion/styled';

export const DataGridContainer = styled.div`
  height: auto;
  width: 100%;
`;
