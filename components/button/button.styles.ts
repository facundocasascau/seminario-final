import styled from '@emotion/styled';
import { Button } from '@mui/material';
import {
  HOVER_PRIMARY_BRAND,
  PRIMARY_BRAND,
  SECONDARY_BRAND,
  TRANSPARENT,
  WHITE
} from 'constants/mainTheme';

export const CustomButton = styled(Button)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  box-sizing: border-box;
  border-radius: 5px;
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 500;
  transition: all 0.25s;

  ${props => props.size && sizes[props.size as keyof typeof sizes]}
  ${props => props.variant && variants[props.variant as keyof typeof variants]}
  ${props => props.color && colors[props.color as keyof typeof colors]}
`;

const variants = {
  contained: `
  border: none;
  color: ${WHITE} !important;

  &:hover {
      filter: saturate(0.65) hue-rotate(3deg);
  }`,
  outlined: `
  background: ${TRANSPARENT} !important;
  border-width: 1px;
  border-style: solid;

  &:hover {
      background: none;
      border: 1px solid ${SECONDARY_BRAND};
  }
  `
};

const colors = {
  primary: `
  background: ${PRIMARY_BRAND};
  border-color: ${PRIMARY_BRAND};
  color: ${WHITE};

  &:hover {
      background: ${HOVER_PRIMARY_BRAND} !important;
      color: ${WHITE};
  }`,
  secondary: `
  background: ${SECONDARY_BRAND};
  border-color: ${SECONDARY_BRAND};
  color: ${SECONDARY_BRAND};

  &:hover {
      background: ${SECONDARY_BRAND} !important;
      color: ${WHITE};
  }`,
  white: `
  background: ${WHITE};
  border-color: ${WHITE};
  color: ${WHITE};

  &:hover {
      background: ${WHITE} !important;
      color: ${SECONDARY_BRAND};
  }`
};

const sizes = {
  large: `
  font-size: 16px;
  line-height: 16px;
  height: 45px;
  padding: 0 30px;
  `,
  medium: `
  font-size: 14px;
  line-height: 14px;
  height: 35px;
  padding: 0 25px;
  `,
  small: `
  font-size: 12px;
  line-height: 12px;
  height: 30px;
  padding: 0 20px;
  `
};
