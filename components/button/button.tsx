import React from 'react';
import { CustomButton } from './button.styles';
import { ButtonProps } from '@mui/material';

export const Index = (props: ButtonProps) => {
  return <CustomButton {...props}>{props.children}</CustomButton>;
};
