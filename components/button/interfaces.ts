import { Url } from "next/dist/shared/lib/router/router";

export interface ButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  color: 'primary' | 'secondary' | 'white';
  size: 'l' | 'm' | 's';
  href: Url;
}

export interface IconButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  image: {
    src: string;
  };
}
