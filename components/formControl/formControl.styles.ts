import styled from '@emotion/styled';
import { HOVER_PRIMARY_BRAND } from 'constants/mainTheme';

export const ContainerControl = styled.div`
  & .MuiInputLabel-root {
    &.Mui-focused {
      color: ${HOVER_PRIMARY_BRAND};
    }
  }
  & .MuiOutlinedInput-root {
    &.Mui-focused fieldset {
      border-color: ${HOVER_PRIMARY_BRAND};
    }
  }
`;
