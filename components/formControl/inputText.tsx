import { Controller } from 'react-hook-form';
import { FormInputProps } from './interfaces';
import { ContainerControl } from './formControl.styles';
import TextField from '@mui/material/TextField';

export const Index = ({ name, control, label, type, multiline, rows }: FormInputProps) => {
  return (
    <ContainerControl>
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <TextField
            helperText={error ? error.message : null}
            error={!!error}
            onChange={onChange}
            value={value}
            label={label}
            fullWidth
            variant="outlined"
            type={type}
            multiline={multiline}
            rows={rows}
          />
        )}
      />
    </ContainerControl>
  );
};
