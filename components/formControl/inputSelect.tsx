import React from 'react';
import { Controller } from 'react-hook-form';
import { FormInputProps, SelectOption } from './interfaces';
import { ContainerControl } from './formControl.styles';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';

export const Index: React.FC<FormInputProps> = ({ name, control, label, options }) => {
  const generateSingleOptions = () => {
    if (!Array.isArray(options) || options.length === 0) {
      return <MenuItem disabled>Sin datos</MenuItem>;
    }
    return options.map((option: SelectOption, index: number) => {
      return (
        <MenuItem
          key={index}
          value={option.value}
          disabled={option.isDisabled}
        >
          {option.label}
        </MenuItem>
      );
    });
  };

  return (
    <ContainerControl>
      <FormControl fullWidth>
        <InputLabel>{label}</InputLabel>
        <Controller
          render={({ field: { onChange, value } }) => (
            <Select
              onChange={onChange}
              value={options?.find(option => option.value === value)?.value || ''}
              label={label}
            >
              {generateSingleOptions()}
            </Select>
          )}
          control={control}
          name={name}
        />
      </FormControl>
    </ContainerControl>
  );
};
