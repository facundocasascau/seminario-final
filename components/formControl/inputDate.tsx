import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { Controller } from 'react-hook-form';
import { FormInputProps } from './interfaces';
import { ContainerControl } from './formControl.styles';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

export const Index = ({ name, control, label }: FormInputProps) => {
  return (
    <ContainerControl>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Controller
          name={name}
          control={control}
          render={({ field: { onChange, value } }) => (
            <DatePicker
              value={value}
              format="dd/MM/yyyy"
              onChange={onChange}
              label={label}
              sx={{ width: '100%' }}
            />
          )}
        />
      </LocalizationProvider>
    </ContainerControl>
  );
};
