export interface FormInputProps {
  name: string;
  control: any;
  label: string;
  setValue?: any;
  type?: string;
  width?: string;
  options?: SelectOption[];
  multiline?: boolean;
  rows?: string | number;
}

export interface SelectOption {
  value: string | ReadonlyArray<string> | number | undefined;
  label: string;
  isDisabled?: boolean;
}
