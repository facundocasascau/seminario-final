import { TabProps } from '@mui/material/Tab';
import { CustomTab } from './tab.styles';

export const Tab = (props: { size?: number } & TabProps) => {
  return <CustomTab {...(props as any)} />;
};
