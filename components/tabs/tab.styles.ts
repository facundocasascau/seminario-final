import styled from '@emotion/styled';
import Tab from '@mui/material/Tab';
import { SECONDARY_BRAND } from 'constants/mainTheme';

export const CustomTab = styled(Tab)((props: any) => {
  const { size, value } = props;
  let borderRadius = '15px';
  if (size > 1) {
    borderRadius = '0';
    if (value === size - 1) {
      borderRadius = '0 15px 15px 0';
    }
    if (value === 0) {
      borderRadius = '15px 0 0 15px';
    }
  }
  return {
    border: '1px solid lightgray',
    borderRadius,
    '&.Mui-selected': {
      color: `${SECONDARY_BRAND}`
    }
  };
});
