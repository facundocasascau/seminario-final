import Tabs from '@mui/material/Tabs';
import { styled } from '@mui/material/styles';
import { SECONDARY_BRAND } from 'constants/mainTheme';

export const CustomTabs = styled(Tabs)({
  '& .MuiTabs-indicator': {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  '& .MuiTabs-indicatorSpan': {
    maxWidth: 50,
    width: '100%',
    backgroundColor: `${SECONDARY_BRAND}`
  }
});
