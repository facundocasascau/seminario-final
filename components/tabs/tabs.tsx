import { ReactElement, ReactNode, cloneElement } from 'react';

import { CustomTabs } from './tabs.styles';
import { TabsProps } from '@mui/material/Tabs';

export const Tabs = ({ children, ...rest }: { children: ReactNode } & TabsProps) => {
  const size = (children as ReactElement[]).length;
  return (
    <CustomTabs
      TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }}
      {...rest}
    >
      {(children as ReactElement[])?.map((child: ReactElement, index: number) => {
        return cloneElement(child, {
          size: size,
          key: index
        });
      })}
    </CustomTabs>
  );
};
