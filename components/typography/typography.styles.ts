import styled from '@emotion/styled';
import { BLACK_TEXT, PRIMARY_BRAND } from 'constants/mainTheme';

export const Title = styled.span`
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 28px;
  line-height: 38px;
  color: ${BLACK_TEXT};
`;

export const Subtitle = styled.span`
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 16px;
  line-height: 26px;
  color: ${PRIMARY_BRAND};
  margin: 0;
`;

export const Text = styled.p`
  font-family: 'Roboto';
  font-weight: ${(props: { $bold: boolean; $italic: boolean }) => (!props.$bold ? 600 : 400)};
  font-style: ${(props: { $bold: boolean; $italic: boolean }) =>
    !props.$italic ? 'italic' : 'normal'};
  font-size: 16px;
  line-height: 26px;
  color: ${BLACK_TEXT};
  margin: 0;
`;
