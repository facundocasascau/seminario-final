import React, { DetailedHTMLProps, HTMLAttributes } from 'react';

import { Subtitle } from './typography.styles';

interface SubtitleProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement> {
  children?: React.ReactNode;
  bold?: boolean;
  italic?: boolean;
}

export const Index = ({ children, ...props }: SubtitleProps) => {
  return <Subtitle {...props}>{children}</Subtitle>;
};
