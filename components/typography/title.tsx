import React from 'react';
import { Title } from './typography.styles';

export const Index = ({ children }: { children: React.ReactNode }) => {
  return <Title>{children}</Title>;
};
