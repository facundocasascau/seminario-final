import React, { DetailedHTMLProps, HTMLAttributes } from 'react';

import { Text } from './typography.styles';

interface TextProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement> {
  children?: React.ReactNode;
  bold?: boolean;
  italic?: boolean;
}

export const Index = ({ children, bold, italic, ...props }: TextProps) => {
  return (
    <Text
      {...props}
      $bold={!bold}
      $italic={!italic}
    >
      {children}
    </Text>
  );
};
