import React from 'react';
import { CardRow } from './iconCard.styles';
import { IconCard } from '.';
import { IconNavLinkProps } from './interfaces';

interface IconCardRowProps {
  cards: Array<IconNavLinkProps>;
}

export const Index = ({ cards }: IconCardRowProps) => {
  return (
    <CardRow>
      {cards.map((card, index) => {
        return (
          <IconCard
            key={index}
            srcImage={card.srcImage}
            srcIcon={card.srcIcon}
            altText={card.altText}
            href={card.href}
            as={card.href}
            cardTitle={card.cardTitle}
          />
        );
      })}
    </CardRow>
  );
};
