import styled from '@emotion/styled';

import Link from 'next/link';
import { HOVER_PRIMARY_BRAND, PRIMARY_BRAND } from 'constants/mainTheme';

export const CardRow = styled.div`
  display: inline-flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: flex-start;
  width: 100%;
  margin-top: 10px;

  @media (max-width: 1023px) {
    display: flex;
    flex-direction: column;

    a {
      max-width: 100% !important;
    }
  }
`;

export const CardNavLink = styled(Link)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  padding: 25px 0px;
  font-family: 'Roboto';
  width: 100%;
  max-width: 250px;
  font-weight: 500;
  font-size: 15px;
  line-height: 18px;
  text-align: left;
  float: right;
  margin: 10px;
  color: ${PRIMARY_BRAND};
  cursor: pointer;
  border-radius: 5px;
  box-sizing: border-box;
  outline: 1px solid ${PRIMARY_BRAND};

  &:hover {
    outline: 2px solid ${HOVER_PRIMARY_BRAND};
  }
`;

export const ImgContainer = styled.div`
  display: flex;
  width: 45px;
  height: 45px;
  margin-left: 15px;
`;

export const TextContainer = styled.div`
  padding-left: 20px;
  width: 160px;
`;
