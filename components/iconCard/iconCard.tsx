import React from 'react';
import Image from 'next/image';
import { CardNavLink, ImgContainer, TextContainer } from './iconCard.styles';
import { IconNavLinkProps } from './interfaces';

export const Index = (iconNavLinkProps: IconNavLinkProps) => {
  const { srcImage, srcIcon: Icon, altText, href, as, cardTitle } = iconNavLinkProps;
  return (
    <CardNavLink
      href={href}
      as={as}
    >
      <ImgContainer>
        {srcImage && (
          <Image
            src={srcImage}
            alt={altText}
            width={45}
          />
        )}
        {Icon && <Icon fontSize="large" />}
      </ImgContainer>
      <TextContainer>{cardTitle}</TextContainer>
    </CardNavLink>
  );
};
