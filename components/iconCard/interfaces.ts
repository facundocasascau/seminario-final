import { OverridableComponent } from '@mui/material/OverridableComponent';
import { SvgIconTypeMap } from '@mui/material/SvgIcon';
import { UrlObject } from 'url';

declare type Url = string | UrlObject;

export interface IconNavLinkProps {
  href: Url;
  as?: Url;
  srcImage?: string;
  srcIcon?: OverridableComponent<SvgIconTypeMap> & { muiName: string };
  altText: string;
  cardTitle?: string;
}
