import { Global as GlobalStyling } from "@emotion/react";
import { GlobalStyles } from "components/global.styles";
import type { AppProps } from "next/app";
import { Layout } from "components";
import 'regenerator-runtime/runtime'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Layout>
      <GlobalStyling styles={GlobalStyles} />
      <Component {...pageProps} />
    </Layout>
  );
}
