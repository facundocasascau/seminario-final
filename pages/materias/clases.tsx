import { useEffect, useRef, useState } from 'react';
import { Button, Menu, MenuItem } from '@mui/material';
import SettingsIcon from '@mui/icons-material/Settings';
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';


const Camera = () => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [quality, setQuality] = useState(0.5);
  const [showControls, setShowControls] = useState(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const startListening = () => SpeechRecognition.startListening({ continuous: true });

  const {
    transcript,
    listening,
    browserSupportsSpeechRecognition
  } = useSpeechRecognition();

  if (!browserSupportsSpeechRecognition) {
    return <div>Browser doesnt support speech recognition.</div>;
  }

  useEffect(() => {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices
        .getUserMedia({ video: { width: { ideal: quality * 1280 } } })
        .then(stream => {
          if (videoRef.current) {
            videoRef.current.srcObject = stream;
          }
        })
        .catch(error => {
          console.error('Error al acceder a la cámara: ' + error);
        });
    }
  }, [quality]);

  useEffect(() => {
    startListening();
  },[]);

  const handleMenuClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <h1>Clase en vivo</h1>
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: '80% 20%',
          width: '100%',
          height: '70vh',
        }}
      >
        <div
          onMouseEnter={() => setShowControls(true)}
          onMouseLeave={() => setShowControls(false)}
          style={{ 
            gridColumn: '1/2',
            height: '100%',
            overflow: 'auto',
            position: 'relative' }}
        >
          <video
            ref={videoRef}
            autoPlay={true}
            onMouseEnter={() => setShowControls(true)}
            //onMouseLeave={() => setShowControls(false)}
          />
          
          
          {showControls && (
            <div
              style={{
                position: 'absolute',
                bottom: 0,
                background: 'rgba(0, 0, 0, 0.7)',
                width: '100%',
                padding: '10px',
                color: '#fff'
              }}
            >
              <Button
                onClick={handleMenuClick}
                startIcon={<SettingsIcon />}
                variant="outlined"
              >
                Configuración
              </Button>
              <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleMenuClose}
              >
                <MenuItem>
                  <label>Calidad de transmisión:</label>
                  <input
                    type="range"
                    min="0.1"
                    max="1"
                    step="0.1"
                    value={quality}
                    onChange={e => setQuality(parseFloat(e.target.value))}
                  />
                  <label>Calidad de transmisión:</label>
                  <button onClick={startListening}>Start</button>
                </MenuItem>
                {/* Agrega más opciones de configuración aquí */}
              </Menu>
              
            </div>
          )}
        </div>
        <div 
          style={{
            gridColumn: '2 / 3',
            height: '100%',
            overflowY: 'scroll',
          }}
        >
          <p>Microphone: {listening ? 'on' : 'off'}</p>
          
          <div>
            <p>{transcript}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Camera;
