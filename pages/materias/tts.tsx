import React, { useState } from 'react';
import pdfjs from 'pdfjs-dist';


interface DocumentInfo {
  name: string;
  path: string;
}

const DocumentsList: React.FC = () => {
  const [selectedDocument, setSelectedDocument] = useState<DocumentInfo | null>(null);
  const [textFromPDF, setTextFromPDF] = useState('');

  const documentos: DocumentInfo[] = [
    { name: 'Documento1', path: '/docs/Descubriendo a Julio Verne.pdf' },
    { name: 'Documento2', path: '/documento2.pdf' },
    // Agrega más documentos según sea necesario
  ];

  const handleDocumentSelect = async (documentInfo: DocumentInfo) => {
    try {
      const response = await fetch(documentInfo.path);
      const arrayBuffer = await response.arrayBuffer();
      const pdf = await pdfjs.getDocument(arrayBuffer).promise;
  
      let fullText = '';
      for (let i = 1; i <= pdf.numPages; i++) {
        const page = await pdf.getPage(i);
        const textContent = await page.getTextContent();
  
        textContent.items.forEach((item : any) => {
          if ('str' in item) {
            fullText += item.str + ' '; // Agregar texto solo si la propiedad 'str' está presente
          }
        });
      }
  
      setTextFromPDF(fullText);
      setSelectedDocument(documentInfo);
    } catch (error) {
      console.error('Error al cargar el archivo PDF:', error);
    }
  };

  return (
    <div>
      <h2>Documentos Disponibles:</h2>
      <ul>
        {documentos.map((doc, index) => (
          <li key={index}>
            <button onClick={() => handleDocumentSelect(doc)}>{doc.name}</button>
          </li>
        ))}
      </ul>
      {selectedDocument && (
        <div>
          <h3>Contenido del documento {selectedDocument.name}:</h3>
          <pre>{textFromPDF}</pre>
        </div>
      )}
    </div>
  );
};

export default DocumentsList;
