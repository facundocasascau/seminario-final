import { GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { LinkButton } from 'components/button';
import { GridContainer, GridV2, PageContainer, Paper, TitleContainer } from 'components/container';
import { DataGrid } from 'components/dataGrid';
import { useGetAgents } from 'lib/services/agents/useGetAgents';
import { useEffect, useState } from 'react';
import { Breadcrumbs } from 'components/breadcrumbs';
import { Icon } from 'components/icons/icons';
import IconButton from '@mui/material/IconButton';
import { useForm } from 'react-hook-form';
import { MateriasHeader } from 'components/operations';
import { asistencia } from 'mocks';
import { CheckCircleOutline, Cancel } from '@mui/icons-material';

interface IFormInput {
  turno: string;
  materia: string;
  //anio: number;
}

const uniqueDates: string[] = asistencia.reduce((dates: string[], item) => {
  if (!dates.includes(item.date)) {
    return [...dates, item.date];
  }
  return dates;
}, []);

export default function Index() {
  const { data: rows, isLoading } = useGetAgents();
  const [filteredData, setFilteredData] = useState(rows);

  const columns: GridColDef[] = [
    {
      field: 'fullName',
      headerName: 'Nombre, Apellido',
      flex: 1,
      valueGetter: (params: GridValueGetterParams) =>
        `${params.row.firstName || ''} ${params.row.lastName || ''}`
    },
    ...uniqueDates.map(date => ({
      field: `date_${date}`,
      headerName: date,
      flex: 1,
      renderCell: (params: any) => {
        const matchingRow = asistencia.find(
          a => a.student_id === params.row.id && a.date === date
        );

        if (matchingRow) {
          const iconStyle: React.CSSProperties = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%'
          };

          return (
            <div style={iconStyle}>
              {matchingRow.is_present ? <CheckCircleOutline /> : <Cancel />}
            </div>
          );
        }

        return null;
      }
    })),
    {
      field: 'action',
      headerName: '',
      minWidth: 20,
      flex: 1,
      maxWidth: 100,
      renderCell: () => {
        return (
          <>
            <IconButton>
              <Icon name="Edit" />
            </IconButton>
          </>
        );
      }
    }
  ];

  const defaultValues = {
    turno: '',
    materia: '',
    anio: ''
  };

  const { control, watch } = useForm<IFormInput>({
    defaultValues: defaultValues
  });

  useEffect(() => {
    setFilteredData(rows);
  }, [rows]);

  return (
    <>
      <Breadcrumbs links={[{ name: 'Home', href: '/' }, { name: 'Materias' }]} />
      <TitleContainer title="Materias" />

      <MateriasHeader
        control={control}
        watch={watch}
      />
      <LinkButton
        href="materias/tts"
        variant="contained"
        size="large"
        color="primary"
      >
        Tareas
      </LinkButton>
      <LinkButton
        href="materias/clases"
        variant="contained"
        size="large"
        color="primary"
      >
        Clases
      </LinkButton>
      <LinkButton
        href="materias/nuevo"
        variant="contained"
        size="large"
        color="primary"
      >
        Grupos
      </LinkButton>
      <PageContainer>
        <Paper>
          <GridContainer>
            <GridV2
              xs={12}
              sx={{
                display: 'flex',
                flexWrap: 'wrap',
                flexDirection: 'column',
                alignItems: 'flex-start'
              }}
            ></GridV2>
          </GridContainer>
        </Paper>
        {isLoading && 'Cargando...'}
        {filteredData && (
          <DataGrid
            rows={filteredData}
            columns={columns}
            sortable={false}
          />
        )}
      </PageContainer>
    </>
  );
}
