import React from 'react';

import Image from 'next/image';
import img404 from 'public/images/img404.svg';
import { HOME_URL } from 'constants/urls';
import { Typography } from '@mui/material';
import { BLACK_TEXT } from 'constants/mainTheme';
import Link from 'next/link';

export default function Index() {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        minHeight: '50vh',
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'flex-end'
      }}
    >
      <Image
        src={img404}
        alt="404"
        height="250"
        width="250"
      />
      <Typography
        sx={{
          fontFamily: 'Poppins',
          fontWeight: '600',
          fontSize: '28px',
          lineHeight: '38px',
          color: BLACK_TEXT,
          margin: '0',
          span: {
            fontWeight: 400
          }
        }}
      >
        La página que estas buscando no se encuentra disponible en este momento.
      </Typography>
      <Link
        color="primary"
        href={HOME_URL}
      >
        Volver al inicio
      </Link>
    </div>
  );
}
