import { Breadcrumbs } from 'components/breadcrumbs';
import { TitleContainer } from 'components/container';

export default function Index() {
  const breadcrumbsLinks = [
    {
      name: 'Home',
      href: '/'
    }
  ];
  return (
    <>
      <Breadcrumbs links={breadcrumbsLinks} />
      <TitleContainer
        title="Under construction"
        subtitle="TBD"
      />
    </>
  );
}
