import { agents } from 'mocks/agents'
import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<typeof agents | { message: string }>
) {
  if(req.method === 'GET') {
    res.status(200).json(agents);
  } else {
    res.status(403).json({ message: 'Method not allowed' });
  }
}
