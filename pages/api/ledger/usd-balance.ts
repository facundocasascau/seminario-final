import { usdBalance } from 'mocks'
import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<typeof usdBalance | { message: string }>
) {
  if(req.method === 'GET') {
    res.status(200).json(usdBalance);
  } else {
    res.status(403).json({ message: 'Method not allowed' });
  }
}
