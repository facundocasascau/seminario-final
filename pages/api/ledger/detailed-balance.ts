import { detailedBalance } from 'mocks'
import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<typeof detailedBalance | { message: string }>
) {
  if(req.method === 'GET') {
    res.status(200).json(detailedBalance);
  } else {
    res.status(403).json({ message: 'Method not allowed' });
  }
}
