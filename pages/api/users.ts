import { users } from 'mocks/users'
import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<typeof users | { message: string }>
) {
  if(req.method === 'GET') {
    res.status(200).json(users);
  } else {
    res.status(403).json({ message: 'Method not allowed' });
  }
}
