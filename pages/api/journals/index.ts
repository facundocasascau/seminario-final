import { journals } from 'mocks'
import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<typeof journals | { message: string }>
) {
  if(req.method === 'GET') {
    res.status(200).json(journals);
  } else {
    res.status(403).json({ message: 'Method not allowed' });
  }
}
