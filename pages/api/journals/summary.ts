import { totalJournal } from 'mocks'
import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<typeof totalJournal | { message: string }>
) {
  if(req.method === 'GET') {
    res.status(200).json(totalJournal);
  } else {
    res.status(403).json({ message: 'Method not allowed' });
  }
}
