import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import { Breadcrumbs } from 'components/breadcrumbs';
import { GridContainer, GridV2, PageContainer, Paper, TitleContainer } from 'components/container';
import { Icon } from 'components/icons';
import { Subtitle, Text } from 'components/typography';
import { CRITICAL_TEXT, SECONDARY_BRAND } from 'constants/mainTheme';
import React from 'react';

export default function Index() {
  const breadcrumbsLinks = [
    {
      name: 'Home',
      href: '/'
    },
    {
      name: 'Panel de admin',
      href: '/panel-admin'
    },
    {
      name: 'Cajas'
    }
  ];

  const boxes = [
    {
      id: 1,
      number: 1,
      status: 'enabled',
      cashier: 'Pepe Grillo',
      operators: ['Pepe Grillo']
    },
    {
      id: 2,
      number: 2,
      status: 'disabled',
      cashier: 'Juana Alavarez',
      operators: []
    },
    {
      id: 3,
      number: 3,
      status: 'enabled',
      cashier: 'Silvina Andreani',
      operators: ['Pedro Gonzalez', 'Juana Castro', 'José Perez']
    },
    {
      id: 4,
      number: 4,
      status: 'disabled',
      cashier: undefined,
      operators: []
    }
  ];
  const statusColor = (status: string) => (status === 'enabled' ? SECONDARY_BRAND : CRITICAL_TEXT);

  return (
    <>
      <Breadcrumbs links={breadcrumbsLinks} />
      <TitleContainer title="Cajas" />
      <PageContainer>
        <GridContainer>
          {boxes.map((box, index) => {
            return (
              <GridV2
                xs={6}
                key={index}
              >
                <Paper
                  style={{
                    minHeight: 240,
                    display: 'flex',
                    flexDirection: 'column',
                    alignContent: 'flex-start',
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start'
                  }}
                >
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Subtitle>{`Caja ${box.number} - Estado: `}</Subtitle>
                    <Subtitle style={{ color: statusColor(box.status), whiteSpace: 'pre-wrap' }}>
                      {box.status === 'enabled' ? ' Habilitada' : ' Deshabilitada'}
                    </Subtitle>
                  </div>
                  <GridContainer>
                    <GridV2 xs={5}>
                      <Stack>
                        <Text bold>Cajero</Text>
                        <Divider orientation="horizontal" />
                        {box.cashier && (
                          <React.Fragment>
                            <div
                              style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                flexDirection: 'row',
                                alignItems: 'center',
                                width: '100%'
                              }}
                            >
                              <Text>{box.cashier}</Text>
                              <IconButton>
                                <Icon name="DoDisturb" />
                              </IconButton>
                            </div>
                            <Divider orientation="horizontal" />
                          </React.Fragment>
                        )}
                      </Stack>
                    </GridV2>
                    <GridV2 xs={1}>
                      {!box.cashier && (
                        <IconButton>
                          <Icon name="AddCircle" />
                        </IconButton>
                      )}
                    </GridV2>
                    <GridV2 xs={5}>
                      <Stack>
                        <Text bold>Operadores</Text>
                        <Divider orientation="horizontal" />
                        {box.operators.map((operator, index) => {
                          return (
                            <React.Fragment key={index}>
                              <div
                                style={{
                                  display: 'flex',
                                  justifyContent: 'space-between',
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  width: '100%'
                                }}
                              >
                                <Text>{operator}</Text>
                                <IconButton>
                                  <Icon name="DoDisturb" />
                                </IconButton>
                              </div>
                              <Divider orientation="horizontal" />
                            </React.Fragment>
                          );
                        })}
                      </Stack>
                    </GridV2>
                    <GridV2 xs={1}>
                      <IconButton>
                        <Icon name="AddCircle" />
                      </IconButton>
                    </GridV2>
                  </GridContainer>
                </Paper>
              </GridV2>
            );
          })}
        </GridContainer>
      </PageContainer>
    </>
  );
}
