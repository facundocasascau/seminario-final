import { Breadcrumbs } from 'components/breadcrumbs';
import { LinkButton, Button } from 'components/button';
import { InputDate, InputSelect, InputText } from 'components/formControl';
import { GridContainer, GridV2, Paper, TitleContainer } from 'components/container';
import { CSSProperties } from 'react';
import { useForm } from 'react-hook-form';

interface IFormInput {
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  sex: string;
  document: string;
  birthdate: Date;
  phoneNumber: string;
  address: string;
}

const rowStyles: CSSProperties = {
  display: 'inline-flex',
  justifyContent: 'flex-start',
  flexDirection: 'row',
  width: '100%',
  gap: '1rem'
};

export default function Index() {
  const breadcrumbsLinks = [
    {
      name: 'Home',
      href: '/'
    },
    {
      name: 'Panel de admin',
      href: '/panel-admin'
    },
    {
      name: 'Usuarios',
      href: '/panel-admin/usuarios'
    },
    {
      name: 'Nuevo usuario'
    }
  ];
  const defaultValues = {
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    sex: '',
    document: '',
    birthdate: undefined,
    phoneNumber: '',
    address: ''
  };
  const { handleSubmit, control } = useForm<IFormInput>({
    defaultValues: defaultValues
  });
  const onSubmit = (data: IFormInput) => console.log(data);

  return (
    <>
      <Breadcrumbs links={breadcrumbsLinks} />
      <TitleContainer title="Nuevo usuario" />

      <Paper>
        <GridContainer>
          <GridV2 xs={6}>
            <InputText
              name="username"
              control={control}
              label="Usuario"
              type="text"
            />
          </GridV2>
          <GridV2 xs={6}>
            <InputText
              name="email"
              control={control}
              label="Email"
              type="email"
            />
          </GridV2>
          <GridV2 xs={3}>
            <InputText
              name="firstName"
              control={control}
              label="Nombre"
              type="text"
            />
          </GridV2>
          <GridV2 xs={3}>
            <InputText
              name="lastName"
              control={control}
              label="Apellido"
              type="text"
            />
          </GridV2>
          <GridV2 xs={3}>
            <InputSelect
              name="sex"
              control={control}
              label="Sexo"
              options={[
                { value: 1, label: 'M - Masculino' },
                { value: 2, label: 'F - Femenino' },
                { value: 3, label: 'X - Otro' }
              ]}
            />
          </GridV2>
          <GridV2 xs={3}>
            <InputText
              name="document"
              control={control}
              label="Documento"
              type="number"
            />
          </GridV2>
          <GridV2 xs={3}>
            <InputDate
              name="birthdate"
              control={control}
              label="Fecha de nacimiento"
            />
          </GridV2>
          <GridV2 xs={3}>
            <InputText
              name="phoneNumber"
              control={control}
              label="Número de teléfono"
              type="data"
            />
          </GridV2>
          <GridV2 xs={6}>
            <InputText
              name="address"
              control={control}
              label="Domicilio"
              type="text"
            />
          </GridV2>
        </GridContainer>
        <div style={rowStyles}>
          <Button
            sx={{ width: '120px' }}
            size="large"
            variant="contained"
            color="primary"
            onClick={handleSubmit(onSubmit)}
          >
            Crear
          </Button>
          <LinkButton
            sx={{ width: '120px' }}
            size="large"
            color="secondary"
            variant="outlined"
            href="/panel-admin/usuarios"
          >
            Cancelar
          </LinkButton>
        </div>
      </Paper>
    </>
  );
}
