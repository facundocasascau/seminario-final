import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { IconButton } from '@mui/material';
import { GridColDef, GridValueGetterParams } from '@mui/x-data-grid';

import { Breadcrumbs } from 'components/breadcrumbs';
import { LinkButton } from 'components/button';
import { TitleContainer } from 'components/container';
import { DataGrid } from 'components/dataGrid';
import { useGetUsers } from 'lib/services/users/useGetUsers';

export default function Index() {
  const breadcrumbsLinks = [
    {
      name: 'Home',
      href: '/'
    },
    {
      name: 'Panel de admin',
      href: '/panel-admin'
    },
    {
      name: 'Usuarios'
    }
  ];
  const columns: GridColDef[] = [
    {
      field: 'username',
      headerName: 'Username',
      maxWidth: 200,
      flex: 1
    },
    {
      field: 'email',
      headerName: 'Email',
      flex: 1,
      maxWidth: 300
    },
    {
      field: 'fullName',
      headerName: 'Nombre, Apellido',
      flex: 1,
      valueGetter: (params: GridValueGetterParams) =>
        `${params.row.firstName || ''} ${params.row.lastName || ''}`
    },
    {
      field: 'action',
      headerName: '',
      minWidth: 20,
      flex: 1,
      maxWidth: 100,
      renderCell: () => {
        return (
          <>
            <IconButton>
              <EditIcon />
            </IconButton>
            <IconButton>
              <DeleteIcon />
            </IconButton>
          </>
        );
      }
    }
  ];

  const { data: rows, isLoading } = useGetUsers();

  return (
    <>
      <Breadcrumbs links={breadcrumbsLinks} />
      <TitleContainer
        title="Usuarios"
        action={
          <LinkButton
            href="/panel-admin/usuarios/nuevo"
            variant="contained"
            size="large"
            color='primary'
          >
            Nuevo
          </LinkButton>
        }
      />
      {isLoading && 'Cargando...'}
      {rows && 
        <DataGrid
          rows={rows}
          columns={columns}
          sortable={false}
        />}
    </>
  );
}
