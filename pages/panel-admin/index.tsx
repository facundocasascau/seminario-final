import { Breadcrumbs } from 'components/breadcrumbs';
import { IconCardRow } from 'components/iconCard';
import iconDarkGreen from 'public/icons/icon-dark-green.svg';
import { TitleContainer } from 'components/container';

export default function Index() {
  const breadcrumbsLinks = [
    {
      name: 'Home',
      href: '/'
    },
    {
      name: 'Panel de admin'
    }
  ];

  const cards = [
    {
      srcImage: iconDarkGreen,
      altText: '1',
      href: '/panel-admin/usuarios',
      cardTitle: 'Usuarios'
    },
    {
      srcImage: iconDarkGreen,
      altText: '1',
      href: '/panel-admin/cajas',
      cardTitle: 'Cajas'
    }
  ];
  return (
    <>
      <Breadcrumbs links={breadcrumbsLinks} />
      <TitleContainer title="Panel de admin" />
      <IconCardRow cards={cards} />
    </>
  );
}
