import Image from 'next/image';
import logo from 'public/logo.png';
import '@babel/polyfill';

export default function Home() {
  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        rowGap: '20px',
        placeContent: 'center flex-start',
        alignItems: 'center'
      }}
    >
      <Image
        alt="EMDEI Logo"
        style={{ opacity: 0.5 }}
        width={480}
        src={logo}
        priority
      />
    </div>
  );
}
