export { agents } from './agents';
export { users } from './users';
export { materias } from './materias';
export { turno } from './turno';
export { configurationMaterias } from './configurationMaterias';
export { anio } from './anio';
export { alumnos } from './alumnos';
export { asistencia } from './asistencia';
