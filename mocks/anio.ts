export const anio = [
    {
      id: 1,
      description: '2020',
    },
    {
      id: 2,
      description: '2021',
    },
    {
      id: 3,
      description: '2022',
    },
    {
      id: 4,
      description: '2023',
    },
  ];
  