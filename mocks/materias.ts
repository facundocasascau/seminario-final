export const materias = [
    {
      id: 1,
      description: 'Ciencias Naturales',
    },
    {
      id: 2,
      description: 'Ciencias Sociales',
    },
    {
      id: 3,
      description: 'Lengua y Literatura',
    },
    {
      id: 4,
      description: 'Matemáticas',
    },
  ];
  