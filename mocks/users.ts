export const users = [
  {
    id: 1,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Snow',
    firstName: 'Jon'
  },
  {
    id: 2,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Lannister',
    firstName: 'Cersei'
  },
  {
    id: 3,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Lannister',
    firstName: 'Jaime'
  },
  {
    id: 4,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Stark',
    firstName: 'Arya'
  },
  {
    id: 5,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Targaryen',
    firstName: 'Daenerys'
  },
  {
    id: 6,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Melisandre',
    firstName: null
  },
  {
    id: 7,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Clifford',
    firstName: 'Ferrara'
  },
  {
    id: 8,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Frances',
    firstName: 'Rossini'
  },
  {
    id: 9,
    username: 'pepegrillo',
    phone: '+543814447542',
    email: 'pepe.grillo@gmail.com',
    lastName: 'Roxie',
    firstName: 'Harvey'
  }
];
