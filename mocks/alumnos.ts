export const alumnos = [
  {
    id: 1,
    phone: '+543814447542',
    lastName: 'Snow',
    firstName: 'Jon',
    sex: 'M',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 2,
    phone: '+543814447542',
    lastName: 'Lannister',
    firstName: 'Cersei',
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 3,
    phone: '+543814447542',
    lastName: 'Lannister',
    firstName: 'Jaime',
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 4,
    phone: '+543814447542',
    lastName: 'Stark',
    firstName: 'Arya',
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 5,
    phone: '+543814447542',
    lastName: 'Targaryen',
    firstName: 'Daenerys',
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 6,
    phone: '+543814447542',
    lastName: 'Melisandre',
    firstName: null,
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 7,
    phone: '+543814447542',
    lastName: 'Clifford',
    firstName: 'Ferrara',
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 8,
    phone: '+543814447542',
    lastName: 'Frances',
    firstName: 'Rossini',
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  },
  {
    id: 9,
    phone: '+543814447542',
    lastName: 'Roxie',
    firstName: 'Harvey',
    sex: 'F',
    identification: '12345678',
    birthDate: '2013-01-01',
    address: 'Av. Siempre Viva 123',
    agentType: 'STUDENT'
  }
];
